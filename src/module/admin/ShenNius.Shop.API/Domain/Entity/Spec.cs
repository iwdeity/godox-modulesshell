﻿using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;

/*************************************
* 类名：Spec
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/9 17:56:52
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Shop.API.Domain.Entity
{
    [SugarTable("shop_Spec")]
    public class Spec : BaseTenantEntity, IAggregateRoot
    {
        public string Name { get; private set; }
        public static Spec Create(string specName, int tenantId)
        {
            var specModel = new Spec
            {
                Name = specName,
                TenantId = tenantId,
            };
            return specModel;
        }
    }
}