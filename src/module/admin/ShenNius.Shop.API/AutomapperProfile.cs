﻿using AutoMapper;
using ShenNius.Shop.API.Domain.Entity;
using ShenNius.Shop.API.Dtos.Input;

namespace ShenNius.Shop.API
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            //shop
            CreateMap<GoodsInput, Goods>();
            CreateMap<GoodsModifyInput, Goods>();
            CreateMap<Goods, GoodsModifyInput>();

            CreateMap<GoodsSpec, GoodsSpecInput>();


            CreateMap<CategoryInput, Category>();
            CreateMap<CategoryModifyInput, Category>();

        }
    }
}
