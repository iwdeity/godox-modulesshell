﻿/*************************************
* 类名：GoodsOutput
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/30 17:28:55
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

using ShenNius.Shop.API.Domain.ValueObjects.Enums;
using ShenNius.Sys.API.Infrastructure.Extension;
using System;

namespace ShenNius.Shop.API.Dtos.Output
{
    public class GoodsOutput
    {
        public string Name { get; set; }
        public string CategoryName { get; set; }
        public int SalesActual { get; set; }
        public int Id { get; set; }
        public string ImgUrl { get; set; }

        public decimal GoodsPrice { get; set; }
        /// <summary>
        /// 商品划线价
        /// </summary>
        public decimal LinePrice { get; set; }

        public int GoodsSales { get; set; }

        public DateTime CreateTime { get; set; }
        public string TenantName { get; set; }

        public int SpecType { get; set; }
        public string SpecTypeText
        {
            get
            {
                string name = "";
                if (SpecType == SpecTypeEnum.Single.GetValue<int>())
                {
                    name = SpecTypeEnum.Single.GetEnumText();
                }
                if (SpecType == SpecTypeEnum.Multi.GetValue<int>())
                {
                    name = SpecTypeEnum.Multi.GetEnumText();
                }
                return name;
            }
        }
        /// <summary>
        /// 库存计算方式
        /// </summary>
        public int DeductStockType { get; set; }
        public string DeductStockTypeText
        {
            get
            {
                string name = "";
                if (DeductStockType == DeductStockTypeEnum.PlaceOrder.GetValue<int>())
                {
                    name = DeductStockTypeEnum.PlaceOrder.GetEnumText();
                }
                if (DeductStockType == DeductStockTypeEnum.Pay.GetValue<int>())
                {
                    name = DeductStockTypeEnum.Pay.GetEnumText();
                }
                return name;
            }
        }


    }
}