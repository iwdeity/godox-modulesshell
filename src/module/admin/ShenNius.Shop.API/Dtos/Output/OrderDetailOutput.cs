﻿using ShenNius.Shop.API.Domain.Entity;
using ShenNius.Shop.API.Domain.ValueObjects.Enums;
using ShenNius.Sys.API.Infrastructure.Extension;
using System;
using System.Collections.Generic;

namespace ShenNius.Shop.API.Dtos.Output
{
    public class OrderDetailOutput
    {
        public int Id { get; set; }
        public string AppUserName { get; set; }
        public DateTime CreateTime { get; set; }
        public string TenantName { get; set; }
        /// <summary>
        /// Desc:订单号
        /// </summary>           
        public string OrderNo { get; set; }

        /// <summary>
        /// Desc:商品总金额
        /// Default:0.00
        /// Nullable:False
        /// </summary>           
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// Desc:订单实付款金额
        /// Default:0.00
        /// Nullable:False
        /// </summary>           
        public decimal PayPrice { get; set; }

        /// <summary>
        /// Desc:付款状态(10未付款 20已付款)
        /// Default:10
        /// Nullable:False
        /// </summary> 
        public int PayStatus { get; set; }
        public string PayStatusText
        {
            get
            {
                string name = "";
                if (PayStatus == PayStatusEnum.WaitForPay.GetValue<int>())
                {
                    name = PayStatusEnum.WaitForPay.GetEnumText();
                }
                if (PayStatus == PayStatusEnum.Paid.GetValue<int>())
                {
                    name = PayStatusEnum.Paid.GetEnumText();
                }
                return name;
            }
        }

        /// <summary>
        /// Desc:付款时间
        /// Default:
        /// Nullable:False
        /// </summary>           
        public DateTime PayTime { get; set; }

        /// <summary>
        /// Desc:运费金额
        /// Default:0.00
        /// Nullable:False
        /// </summary>           
        public decimal ExpressPrice { get; set; }

        /// <summary>
        /// Desc:物流公司
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string ExpressCompany { get; set; }

        /// <summary>
        /// Desc:物流单号
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string ExpressNo { get; set; }

        /// <summary>
        /// Desc:发货状态(10未发货 20已发货)
        /// Default:10
        /// Nullable:False
        /// </summary>           
        public int DeliveryStatus { get; set; }
        public string DeliveryStatusText
        {
            get
            {
                string name = "";
                if (DeliveryStatus == DeliveryStatusEnum.WaitForSending.GetValue<int>())
                {
                    name = DeliveryStatusEnum.WaitForSending.GetEnumText();
                }
                if (DeliveryStatus == DeliveryStatusEnum.Sended.GetValue<int>())
                {
                    name = DeliveryStatusEnum.Sended.GetEnumText();
                }
                return name;
            }
        }

        /// <summary>
        /// Desc:发货时间
        /// Default:
        /// Nullable:False
        /// </summary>           
        public DateTime DeliveryTime { get; set; }

        /// <summary>
        /// Desc:收货状态(10未收货 20已收货)
        /// Default:10
        /// Nullable:False
        /// </summary>           
        public int ReceiptStatus { get; set; }
        /// <summary>
        ///   收获时间
        /// </summary>
        public DateTime ReceiptTime { get; set; }

        public string ReceiptStatusText
        {
            get
            {
                string name = "";
                if (ReceiptStatus == ReceiptStatusEnum.WaitForReceiving.GetValue<int>())
                {
                    name = ReceiptStatusEnum.WaitForReceiving.GetEnumText();
                }
                if (ReceiptStatus == ReceiptStatusEnum.Received.GetValue<int>())
                {
                    name = ReceiptStatusEnum.Received.GetEnumText();
                }
                return name;
            }
        }


        /// <summary>
        /// Desc:订单状态(10进行中 20取消 21待取消 30已完成)
        /// Default:10
        /// Nullable:False
        /// </summary>           
        public int OrderStatus { get; set; }
        /// <summary>
        /// Desc:微信支付交易号
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string TransactionId { get; set; }

        /// <summary>
        /// Desc:用户id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public int AppUserId { get; set; }
        public OrderAddress Address { get; set; } = new OrderAddress();
        public List<OrderGoodsDetailOutput> GoodsDetailList { get; set; } = new List<OrderGoodsDetailOutput>();
    }
    public class OrderGoodsDetailOutput
    {
        public string GoodsImg { get; set; }
        public string GoodsName { get; set; }
        public string GoodsAttr { get; set; }
        public int GoodsId { get; set; }
        public decimal GoodsPrice { get; set; }
        public double GoodsWeight { get; set; }
        public decimal TotalNum { get; set; }
        public decimal TotalPrice { get; set; }
        public DateTime CreateTime { get; set; }
        public string GoodsNo { get; set; }
    }
}
