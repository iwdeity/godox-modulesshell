﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Repository.Extensions;
using ShenNius.Shop.API.Domain.Entity;
using ShenNius.Shop.API.Dtos.Output;
using ShenNius.Sys.API.Controllers;
using ShenNius.Sys.API.Domain.Entity;
using ShenNius.Sys.API.Dtos.Common;
using ShenNius.Sys.API.Infrastructure.Attributes;
using ShenNius.Sys.API.Infrastructure.Configs;
using SqlSugar;
using System.Threading.Tasks;

/*************************************
* 类名：AppUserController
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/23 11:01:15
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Shop.API.Controllers
{
    [Route("api/shop/[controller]/[action]")]
    [MultiTenant]
    public class AppUserAddressController : ApiControllerBase
    {
        private readonly ISqlSugarClient _db;
        public AppUserAddressController(ISqlSugarClient db)
        {
            _db = db;
        }
        [HttpGet, Authority]
        public async Task<ApiResult> GetListPages([FromQuery] KeyListTenantQuery query)
        {
            var data = await _db.Queryable<AppUserAddress, AppUser>((d, u) => new object[] { JoinType.Inner,d.Id==u.AddressId&&d.IsDeleted
            }).Where((d, u) => d.TenantId == query.TenantId).WhereIF(!string.IsNullOrEmpty(query.Key), (d, u) => d.Name.Equals(query.Key))
                .OrderBy((d, u) => d.Id, OrderByType.Desc)
                .Select((d, u) => new AppUserAddressOutput()
                {
                    Name = d.Name,
                    Province = d.Province,
                    City = d.City,
                    Detail = d.Detail,
                    Region = d.Region,
                    CreateTime = d.CreateTime,
                    AppUserName = u.NickName,
                    TenantName = SqlFunc.Subqueryable<Tenant>().Where(s => s.Id == d.TenantId).Select(s => s.Name),
                }).ToPageAsync(query.Page, query.Limit);
            return new ApiResult(data);
        }
    }
}