﻿using System;

namespace ShenNius.Sys.API.Domain.Entity.Common
{
    public interface IHasDeleteTime
    {
        DateTime? DeleteTime { get; }
    }
}
