﻿using SqlSugar;
using System;

namespace ShenNius.Sys.API.Domain.Entity.Common
{
    /// <summary>
    ///多租户领域基类
    /// </summary>
    public  class BaseTenantEntity : IGlobalTenant, IEntity, IHasModifyTime, IHasCreateTime, IHasDeleteTime, ISoftDelete
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; protected set; }
        public int TenantId { get; set; }
        [SugarColumn(IsIgnore = true)]
        public string TenantName { get; set; }
        public DateTime? ModifyTime { get; protected set; }
        public DateTime CreateTime { get; protected set; } = DateTime.Now;
        public bool IsDeleted { get; private set; }
        public DateTime? DeleteTime { get; private set; }
        public void SoftDelete()
        {
            this.IsDeleted = true;
            this.DeleteTime = DateTime.Now;
        }
        public void NotifyModified()
        {
            this.ModifyTime = DateTime.Now;
        }       
    }

    public  class BaseTenantTreeEntity : BaseTenantEntity
    { 
         public  string ParentList { get; protected set; }
        /// Desc:栏位等级     
        public  int Layer { get; protected set; }
        public  int ParentId { get; protected set; }
        public void ChangeParentList(int layer, string parentList)
        {
            Layer = layer;
            ParentList = parentList;
            NotifyModified();
        }

       
    }
}
