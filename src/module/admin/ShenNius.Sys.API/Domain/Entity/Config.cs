﻿using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Sys.API.Domain.Entity
{
    ///<summary>
    /// 字典表
    ///</summary>
    [SugarTable("Sys_Config")]
    public partial class Config : BaseEntity, IAggregateRoot
    {
        public Config()
        {

        }


        /// <summary>
        /// Desc:字典值——名称
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Value { get; private set; }

        public void ChangeValue(string value, string key = null)
        {
            Value = value;
            if (!string.IsNullOrEmpty(key))
            {
                Type = key;
            }
        }

        /// <summary>
        /// Desc:字典值——英文名称
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string EnName { get; private set; }

        /// <summary>
        /// Desc:字典值——描述
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Remark { get; private set; }
        public string Type { get; private set; }

        public void Modify(string name, string enName, string type, string summary, int id)
        {
            Value = name;
            EnName = enName;
            Type = type;
            Remark = summary;
            Id = id;
            NotifyModified();
        }
    }
}
