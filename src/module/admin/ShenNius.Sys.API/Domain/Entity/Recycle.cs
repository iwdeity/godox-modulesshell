﻿using ShenNius.Sys.API.Domain.Entity.Common;
using SqlSugar;
using System;

/*************************************
* 类名：Recycle
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/4/8 18:01:45
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Sys.API.Domain.Entity
{
    /// <summary>
    /// 回收站
    /// </summary>
    [SugarTable("Sys_Recycle")]
    public class Recycle : IAggregateRoot
    {
        public Recycle()
        {

        }
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; protected set; }
        public int UserId { get; private set; }
        public int TenantId { get; private set; }
        public int BusinessId { get; private set; }
        public string TableType { get; private set; }
        public DateTime CreateTime { get; private set; }
        public string Remark { get; private set; }
        /// <summary>
        /// 要还原的数据库sql
        /// </summary>
        public string RestoreSql { get; private set; }
        public string RealyDelSql { get; private set; }
        public static Recycle Create(int businessId, int userId, string tableName, int tenantId, string currentName,bool isTenant=true)
        {
            var recycle = new Recycle()
            {
                CreateTime = DateTime.Now,
                BusinessId = businessId,
                UserId = userId,
                TableType = tableName,
                TenantId = tenantId,
                Remark = $"用户名为【{currentName}】删除了表【{tableName}】中id={businessId}的记录数据。",
              
            };
            if (isTenant)
            {
                recycle.RestoreSql = $"update {tableName} set IsDeleted=false where id={businessId} and TenantId={tenantId}";
                recycle.RealyDelSql = $"delete  from {tableName}  where id={businessId} and TenantId={tenantId}";
            }
            else {
                recycle.RestoreSql = $"update {tableName} set IsDeleted=false where id={businessId} ";
                recycle.RealyDelSql = $"delete  from {tableName}  where id={businessId} ";
            }
            return recycle;
        }
        
    }
}