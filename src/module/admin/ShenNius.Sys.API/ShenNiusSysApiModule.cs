﻿using Microsoft.Extensions.DependencyInjection;
using ShenNius.FileManagement;
using ShenNius.ModuleCore;
using ShenNius.ModuleCore.Context;
using ShenNius.ModuleCore.Extensions;
using ShenNius.Repository;
using ShenNius.Sys.API.Domain.Repository;
using ShenNius.Sys.API.Domain.Service;
using ShenNius.Sys.API.Hubs;
using ShenNius.Sys.API.Infrastructure;
using ShenNius.Sys.API.Infrastructure.Common;
using ShenNius.Sys.API.Jwt;
using ShenNius.Sys.API.Jwt.Extension;
using ShenNius.Sys.API.TimedTask;
using SqlSugar;
using System;


namespace ShenNius.Sys.API
{

    public class ShenNiusSysApiModule : AppModule
    {
        public override void OnConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddSqlsugarSetup(option =>
            {
                option.ConnectionString = context.Configuration["ConnectionStrings:MySql"];
                option.DbType = DbType.MySql;
                option.IsAutoCloseConnection = true;
                option.InitKeyType = InitKeyType.Attribute;//从特性读取主键自增信息
            });
            context.Services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            context.Services.AddAutoMapper(typeof(AutomapperProfile));
            var jwtEnable = !string.IsNullOrEmpty(context.Configuration["JwtConfig:IsEnable"]) ? Convert.ToBoolean(context.Configuration["JwtConfig:IsEnable"]) : true;
            if (jwtEnable)
            {
                //context.Services.AddSwaggerSetup();
                //注入MiniProfiler
                // context.Services.AddMiniProfiler(options =>
                //     options.RouteBasePath = "/profiler"
                //);
                context.Services.AddAuthorizationSetup(context.Configuration);
            }
            context.Services.AddScoped<IMenuRepository, MenuRepository>();
            context.Services.AddScoped<IRecycleRepository, RecycleRepository>();
            context.Services.AddScoped<ICurrentUserContext, CurrentUserContext>();
            WebHelper.allDbTable = context.Configuration["DbTable:Value"];
            context.Services.AddScoped<JwtHelper>();
            context.Services.AddScoped<LoginDomainService>();
            context.Services.AddScoped<UserLoginNotifiHub>();
            context.Services.AddHostedService<TimedBackgroundService>();
            MyHttpContext.ServiceProvider = context.Services.BuildServiceProvider();

            context.Services.AddFileUpload(option =>
            {
                option.FileType = FileType.Local;
            });
            context.Services.AddDistributedMemoryCache();
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();
            //加入健康检查中间件
            // app.UseHealthChecks("/health");
            NLog.LogManager.LoadConfiguration("nlog.config").GetCurrentClassLogger();
            NLog.LogManager.Configuration.Variables["connectionString"] = context.Configuration["ConnectionStrings:MySql"];
        }
    }
}
