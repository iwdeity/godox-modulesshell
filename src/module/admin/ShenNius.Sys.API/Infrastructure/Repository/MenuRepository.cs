﻿using AutoMapper;
using Microsoft.Extensions.Caching.Distributed;
using ShenNius.Caches;
using ShenNius.Repository;
using ShenNius.Repository.Extensions;
using ShenNius.Sys.API.Domain.Entity;
using ShenNius.Sys.API.Domain.Repository;
using ShenNius.Sys.API.Domain.ValueObjects;
using ShenNius.Sys.API.Domain.ValueObjects.Enum;
using ShenNius.Sys.API.Dtos.Input;
using ShenNius.Sys.API.Dtos.Output;
using ShenNius.Sys.API.Infrastructure.Common;
using ShenNius.Sys.API.Infrastructure.Configs;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShenNius.Sys.API.Infrastructure
{
    public class MenuRepository : BaseRepository<Menu>, IMenuRepository
    {
        private readonly IMapper _mapper;
        private readonly IDistributedCache _cache;
        public MenuRepository(ISqlSugarClient db, IMapper mapper, IDistributedCache cache) : base(db)
        {
            _mapper = mapper;
            _cache = cache;
        }
        public async Task<List<MenuAuthOutput>> GetCurrentAuthMenus(int userId)
        {
            var data = _cache.Get<List<MenuAuthOutput>>($"{SysCacheKey.AuthMenu}:{userId}");
            if (data == null)
            {
                var allRoleMenus = await GetCurrentMenuByUser(userId);
                var allMenuIds = allRoleMenus.Select(d => d.MenuId).ToList();
                var configs = await db.Queryable<Config>().Where(m => m.Type == nameof(ConfigEnum.Button) && m.IsDeleted == false).ToListAsync();
                var query = await db.Queryable<Menu>().Where(d => d.IsDeleted == false).WhereIF(allMenuIds.Count > 0, d => allMenuIds.Contains(d.Id)).ToListAsync();

                foreach (var item in query)
                {
                    var model = allRoleMenus.FirstOrDefault(d => d.MenuId == item.Id);
                    if (model != null)
                    {
                        if (item.BtnCodeIds == null || item.BtnCodeIds.Length <= 0)
                        {
                            continue;
                        }
                        item.BtnCodeIds = model.BtnCodeIds;
                        if (item.BtnCodeIds != null && item.BtnCodeIds.Length > 0)
                        {
                            foreach (var itemBtn in item.BtnCodeIds)
                            {
                                //1、已经授权的按钮id匹配字典表找出按钮名称
                                if (!string.IsNullOrEmpty(itemBtn))
                                {
                                    var configModel = configs.FirstOrDefault(d => d.Id.ToString().Equals(itemBtn));
                                    if (configModel != null)
                                    {
                                        item.BtnCodeName += configModel.EnName + ",";
                                    }
                                }
                            }
                            if (!string.IsNullOrEmpty(item.BtnCodeName))
                            {
                                item.BtnCodeName = item.BtnCodeName.TrimEnd(',');
                            }
                        }
                    }
                }

                //把当前用户拥有的权限存入到缓存里面
                data = _mapper.Map<List<MenuAuthOutput>>(query);
                _cache.Set($"{SysCacheKey.AuthMenu}:{userId}", data);
            }
            return data;
        }
        public async Task<ApiResult> GetListPagesAsync(int page, string key = null)
        {
            var res = await db.Queryable<Menu>().Where(d => !d.IsDeleted).WhereIF(!string.IsNullOrEmpty(key), d => d.Name.Contains(key))
                      .OrderBy(m => m.CreateTime, SqlSugar.OrderByType.Desc)
                          .Mapper((it, cache) =>
                          {
                              var codeList = cache.Get(t =>
                              {
                                  return db.Queryable<Config>().Where(m => m.Type == nameof(ConfigEnum.Button) && !m.IsDeleted).ToList();
                              });
                              var list = new List<string>();
                              if (it.BtnCodeIds != null)
                              {
                                  if (it.BtnCodeIds.Length > 0)
                                  {
                                      list = it.BtnCodeIds.ToList();
                                  }
                              }
                              if (list.Count > 0)
                              {
                                  it.BtnCodeName = string.Join(',', codeList.Where(g => list.Contains(g.Id.ToString())).Select(g => g.Value).ToList());
                              }
                          })
                   //这里的一页1500条 是为了显示菜单树形的关系。
                   .ToPageAsync(page, 2000);

            var result = new List<Menu>();
            if (!string.IsNullOrEmpty(key))
            {
                var menuModel = await GetModelAsync(m => m.Name.Contains(key));
                ChildModule(res.Items, result, menuModel.ParentId);
            }
            else
            {
                ChildModule(res.Items, result, 0);
            }

            if (result?.Count > 0)
            {
                foreach (var item in result)
                {
                    var name = WebHelper.LevelName(item.Name, item.Layer);
                    item.ChangeName(name);
                }
            }
            return new ApiResult(data: new { count = res.TotalItems, items = result });
        }
        /// <summary>
        /// 递归模块列表
        /// </summary>
        private void ChildModule(List<Menu> list, List<Menu> newlist, int parentId)
        {
            var result = list.Where(p => p.ParentId == parentId).OrderBy(p => p.Layer).ThenBy(p => p.Sort).ToList();
            if (!result.Any()) return;
            for (int i = 0; i < result.Count(); i++)
            {
                newlist.Add(result[i]);
                ChildModule(list, newlist, result[i].Id);
            }
        }

        public async Task<ApiResult> GetAllParentMenuAsync()
        {
            var list = await GetListAsync(d => !d.IsDeleted);
            var data = new List<Menu>();
            ChildModule(list, data, 0);

            if (data?.Count > 0)
            {
                foreach (var item in data)
                {
                    var name = WebHelper.LevelName(item.Name, item.Layer);
                    item.ChangeName(name);
                }
            }
            return new ApiResult(data);
        }

        public async Task<ApiResult> AddToUpdateAsync(MenuInput menuInput)
        {
            var menuModel = await GetModelAsync(d => d.NameCode.Equals(menuInput.NameCode));
            if (menuModel?.Id > 0)
            {
                throw new ArgumentNullException("已经存在该权限码了");
            }
            var menu = _mapper.Map<Menu>(menuInput);
            var menuId = await AddAsync(menu);
            menu.ChangeId(menuId);
            string parentIdList = ""; int layer = 0;
            if (menuInput.ParentId > 0)
            {
                // 说明有父级  根据父级，查询对应的模型
                var model = await GetModelAsync(d => d.Id == menuInput.ParentId);
                if (model.Id > 0)
                {
                    parentIdList = model.ParentIdList + menuId + ",";
                    layer = model.Layer + 1;
                }
            }
            else
            {
                parentIdList = "," + menuId + ",";
                layer = 1;
            }
            menu.ChangeParentIdList(parentIdList, layer);
            await UpdateAsync(menu);
            return new ApiResult();
        }

        public async Task<ApiResult> ModifyAsync(MenuModifyInput input)
        {
            var menuModel = await GetModelAsync(d => d.NameCode.Equals(input.NameCode) && d.Id != input.Id && d.IsDeleted == false);
            if (menuModel?.Id > 0)
            {
                throw new ArgumentNullException("已经存在该权限码了");
            }
            string parentIdList = ""; int layer = 0;
            if (input.ParentId > 0)
            {
                // 说明有父级  根据父级，查询对应的模型
                var model = await GetModelAsync(d => d.Id == input.ParentId && d.IsDeleted == false);
                if (model.Id > 0)
                {
                    parentIdList = model.ParentIdList + input.Id + ",";
                    layer = model.Layer + 1;
                }
            }
            else
            {
                parentIdList = "," + input.Id + ",";
                layer = 1;
            }
            var menu = _mapper.Map<Menu>(input);
            menu.ChangeParentIdList(parentIdList, layer);
            await UpdateAsync(menu);
            return new ApiResult();
        }

        /// <summary>
        /// 根据菜单id获取关联按钮
        /// </summary>
        /// <param name="menuId"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public async Task<ApiResult> BtnCodeByMenuIdAsync(int menuId, int roleId)
        {
            if (menuId == 0)
            {
                return new ApiResult(menuId);
            }
            var menuModel = await GetModelAsync(d => !d.IsDeleted && d.Id == menuId);
            if (menuModel == null)
            {
                throw new ArgumentNullException(nameof(menuModel));
            }
            if (menuModel.BtnCodeIds.Length <= 0 || menuModel.BtnCodeIds == null)
            {
                return new ApiResult(menuId);
            }
            var containsBtnList = await db.Queryable<Config>().Where(d => menuModel.BtnCodeIds.Contains(d.Id.ToString()) && d.IsDeleted == false).Select(d => new ConfigBtnOutput()
            {
                Id = d.Id,
                Name = d.Value,
                Status = false
            }).ToListAsync();

            var permissionsModel = await db.Queryable<R_Role_Menu>().Where(d => d.RoleId == roleId && d.MenuId == menuId && d.IsDeleted == false).FirstAsync();

            if (permissionsModel != null && permissionsModel.BtnCodeIds != null)
            {
                if (permissionsModel.BtnCodeIds.Length > 0)
                {
                    foreach (var item in containsBtnList)
                    {
                        if (permissionsModel.BtnCodeIds.Contains(item.Id.ToString()) && permissionsModel.BtnCodeIds != null)
                        {
                            item.Status = true;
                        }
                    }
                }
            }
            return new ApiResult(containsBtnList);
        }
        // [CacheInterceptor]
        public async Task<ApiResult> TreeRoleIdAsync(int roleId)
        {
            var list = new List<MenuTreeOutput>();
            var existMenuId = await db.Queryable<R_Role_Menu>().Where(d => d.RoleId == roleId && d.IsDeleted == false).Select(d => d.MenuId).ToListAsync();

            var allMenus = await GetListAsync(d => !d.IsDeleted);
            if (allMenus.Count <= 0 || allMenus == null)
            {
                return new ApiResult(allMenus);
            }

            foreach (var item in allMenus)
            {
                if (item.ParentId != 0)
                {
                    continue;
                }
                var menuTreeOutput = new MenuTreeOutput()
                {
                    Id = item.Id,
                    Title = item.Name,
                    Checked = existMenuId.FirstOrDefault(d => d == item.Id) > 0,
                    Children = AddChildNode(allMenus, item.Id, existMenuId),
                };
                list.Add(menuTreeOutput);
            }
            return new ApiResult(list);
        }
        private List<MenuTreeOutput> AddChildNode(IEnumerable<Menu> data, int parentId, List<int> existMenuId)
        {
            var list = new List<MenuTreeOutput>();
            var data2 = data.Where(d => d.ParentId == parentId).OrderBy(d => d.Name).ToList();
            foreach (var item in data2)
            {
                var menuTreeOutput = new MenuTreeOutput()
                {
                    Id = item.Id,
                    Title = item.Name,
                    Checked = existMenuId.FirstOrDefault(d => d == item.Id) > 0,
                    Children = AddChildNode(data, item.Id, existMenuId)
                };
                list.Add(menuTreeOutput);
            }
            return list;
        }

        public async Task<List<R_Role_Menu>> GetCurrentMenuByUser(int userId)
        {
            var allRoleIds = await db.Queryable<R_User_Role>().Where(d => d.UserId == userId && !d.IsDeleted).Select(d => d.RoleId).ToListAsync();
            if (allRoleIds == null || allRoleIds.Count == 0)
            {
                throw new ArgumentException("当前用户没有角色授权");
            }
            var allRoleMenus = await db.Queryable<R_Role_Menu>().Where(d => allRoleIds.Contains(d.RoleId) && d.IsDeleted == false).ToListAsync();
            if (allRoleMenus == null || allRoleMenus.Count == 0)
            {
                throw new ArgumentException("当前角色没有菜单授权");
            }
            return allRoleMenus;
        }
        // [CacheInterceptor]
        public async Task<ApiResult> LoadLeftMenuTreesAsync(int userId)
        {
            var value = _cache.Get<SysSetting>(nameof(ConfigEnum.Setting));
            var allRoleMenus = await GetCurrentMenuByUser(userId);
            var allMenuIds = allRoleMenus.Select(d => d.MenuId).ToList();
            var allMenus = await GetListAsync(d => d.IsDeleted == false && allMenuIds.Contains(d.Id));

            var model = new MenuTreeInitOutput()
            {
                HomeInfo = new HomeInfo() { Title = "首页", Href = "/home/report" },

                LogoInfo = new LogoInfo() { Title = "神牛系统平台", Image = "/images/logo.png?v=999", Href="#" },
            };
            if (value != null && !string.IsNullOrEmpty(value.Name))
            {
                model.LogoInfo.Title = value.Name;
                if (string.IsNullOrEmpty(value.Logo))
                {
                    model.LogoInfo.Image = value.Logo;
                }

            }
            //if (!AppSettings.Jwt.Value)
            //{
            //    model.HomeInfo = new HomeInfo() { Title = "首页", Href = "/sys/logs/echarts" };
            //}
            List<MenuInfo> menuInfos = new List<MenuInfo>();
            foreach (var item in allMenus)
            {
                if (item.ParentId != 0)
                {
                    continue;
                }
                var menuInfo = new MenuInfo()
                {
                    Title = item.Name,
                    Icon = item.Icon,
                    Target = "_self",
                    Href = item.Url,
                    Child = AddMenuChildNode(allMenus, item.Id)
                };
                menuInfos.Add(menuInfo);
            }
            model.MenuInfo = menuInfos;
            return new ApiResult(model);
        }
        private List<MenuInfo> AddMenuChildNode(List<Menu> data, int parentId)
        {
            var list = new List<MenuInfo>();
            var data2 = data.Where(d => d.ParentId == parentId).ToList();
            foreach (var item in data2)
            {
                var menuTreeOutput = new MenuInfo()
                {
                    Title = item.Name,
                    Icon = item.Icon,
                    Target = "_self",
                    Href = item.Url,
                    Child = AddMenuChildNode(data, item.Id)
                };
                list.Add(menuTreeOutput);
            }
            return list;
        }
    }
}
