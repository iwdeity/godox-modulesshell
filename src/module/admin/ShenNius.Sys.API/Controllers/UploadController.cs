﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.FileManagement;
using ShenNius.Sys.API.Infrastructure.Configs;


namespace ShenNius.Sys.API.Controllers
{
    /// <summary>
    /// 商品分类控制器
    /// </summary>
    [Route("api/sys/[controller]/[action]")]
    public class UploadController : ApiControllerBase
    {
        private readonly IUploadFile _uploadHelper;
        public UploadController(IUploadFile uploadHelper)
        {
            _uploadHelper = uploadHelper;
        }
        [HttpPost]
        public ApiResult Files([FromForm] string directory)
        {
            var files = Request.Form.Files;
            var datas = _uploadHelper.Upload(files, directory);
            if (datas.Count > 0)
            {
                return new ApiResult(datas);
            }
            return new ApiResult("上传失败！");
        }
        [HttpPost]
        public ApiResult File([FromForm] string directory)
        {
            var file = Request.Form.Files[0];
            var data = _uploadHelper.Upload(file, directory);
            if (!string.IsNullOrEmpty(data))
            {
                var rssult = new ApiResult(data: data);
                return rssult;
            }
            return new ApiResult("上传失败！");
        }
    }
}
