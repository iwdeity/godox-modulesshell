﻿namespace ShenNius.Sys.API.Dtos.Input
{
    public class SetUserRoleInput
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public bool Status { get; set; } = true;
    }
}
