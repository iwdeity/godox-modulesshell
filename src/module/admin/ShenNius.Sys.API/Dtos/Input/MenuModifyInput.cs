﻿using System;

namespace ShenNius.Sys.API.Dtos.Input
{
    public class MenuModifyInput
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public string NameCode { get; set; }
        public string Url { get; set; }
        public string HttpMethod { get; set; }
        public DateTime ModifyTime { get; set; } = DateTime.Now;
        public int Sort { get; set; }
        public string Icon { get; set; }
        public string[] BtnCodeIds { get; set; }
    }
}
