﻿using System.Collections.Generic;

namespace ShenNius.Sys.API.Dtos.Input
{
    public class SetRoleMenuInput
    {
        public int RoleId { get; set; }
        public List<int> MenuIds { get; set; }
    }
}
