﻿using System;

namespace ShenNius.Sys.API.Dtos.Input
{
    public class ConfigInput
    {
        public string Name { get; set; }

        /// <summary>
        /// Desc:字典值——英文名称
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string EnName { get; set; }

        /// <summary>
        /// Desc:字典值——描述
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Remark { get; set; }

        public string Type { get; set; }
        public DateTime CreateTime { get; set; } 
    }
}
