﻿using ShenNius.Sys.API.Domain.Entity.Common;
using System.Collections.Generic;

namespace ShenNius.Sys.API.Dtos.Common
{
    public class DeletesInput
    {
        public List<int> Ids { get; set; } = new List<int>();
    }
    public class DeletesTenantInput : DeletesInput, IGlobalTenant
    {

        public int TenantId { get; set; }
    }
}
