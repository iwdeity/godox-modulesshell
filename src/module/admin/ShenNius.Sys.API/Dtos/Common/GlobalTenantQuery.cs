﻿using ShenNius.Sys.API.Domain.Entity.Common;

namespace ShenNius.Sys.API.Dtos.Common
{
    public class GlobalTenantQuery : IGlobalTenant
    {
        public int TenantId { get; set; }
    }
}
