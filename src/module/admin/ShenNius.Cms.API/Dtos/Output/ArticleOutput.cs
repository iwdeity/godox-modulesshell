﻿using Magicodes.ExporterAndImporter.Core;
using System;

namespace ShenNius.Cms.API.Dtos.Output
{
    public class ArticleOutput
    {
        [ExporterHeader(DisplayName = "文章标题")]
        public string Title { get; set; }
        [ExporterHeader(DisplayName = "创建时间")]
        public DateTime CreateTime { get; set; }
        [ExporterHeader(DisplayName = "修改时间")]
        public DateTime? ModifyTime { get; set; }
        [ExporterHeader(DisplayName = "审核状态")]
        public bool Audit { get; set; }
        [ExporterHeader(DisplayName = "作者")]
        public string Author { get; set; }
        [ExporterHeader(DisplayName = "来源")]
        public string Source { get; set; }
        [ExporterHeader(DisplayName = "文章栏目")]
        public string ColumnName { get; set; }

    }
}
