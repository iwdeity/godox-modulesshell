﻿using AutoMapper;
using ShenNius.Cms.API.Domain.Entity;
using ShenNius.Cms.API.Dtos.Input;
using ShenNius.Cms.API.Dtos.Output;

namespace ShenNius.Cms.API
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            //cms
            CreateMap<ColumnInput, Column>();
            CreateMap<ColumnModifyInput, Column>();

            CreateMap<ArticleInput, Article>();
            CreateMap<ArticleModifyInput, Article>();

            CreateMap<AdvListInput, AdvList>();
            CreateMap<AdvListModifyInput, AdvList>();

            CreateMap<KeywordInput, Keyword>();
            CreateMap<KeywordModifyInput, Keyword>();

            CreateMap<Keyword, KeywordOutput>();

        }
    }
}
