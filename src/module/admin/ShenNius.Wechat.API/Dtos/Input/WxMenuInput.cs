﻿namespace ShenNius.Wechat.API.Dtos.Input
{
    public class WxMenuInput
    {
        public int Id { get; set; }
        public string Menu { get; set; }
    }
}
