﻿using ShenNius.Auth.API.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Auth.API.Models.Entity
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("shop_appuser")]
    public class AppUser : BaseTenantEntity, IAggregateRoot
    {
        private AppUser()
        {

        }
        /// <summary>
        /// Desc:微信openid(唯一标示)
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string OpenId { get; private set; }

        /// <summary>
        /// Desc:微信昵称
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string NickName { get; private set; }

        /// <summary>
        /// Desc:微信头像
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string AvatarUrl { get; private set; }

        /// <summary>
        /// Desc:性别
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public byte Gender { get; private set; }

        /// <summary>
        /// Desc:国家
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Country { get; private set; }

        /// <summary>
        /// Desc:省份
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Province { get; private set; }

        /// <summary>
        /// Desc:城市
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string City { get; private set; }

        /// <summary>
        /// Desc:默认收货地址
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public int AddressId { get; private set; }

        public static AppUser Create(string nickName, byte gender, string avatarUrl, string country, string province, string city, string openId, int tenantId)
        {
            AppUser appUser = new AppUser();
            appUser.NickName = nickName;
            appUser.Gender = gender;
            appUser.AvatarUrl = avatarUrl;
            appUser.Country = country;
            appUser.Province = province;
            appUser.City = city;
            appUser.OpenId = openId;
            appUser.TenantId = tenantId;
            return appUser;
        }
        public void ModifyAddressId(int addressId)
        {
            AddressId = addressId;
        }
    }
}
