﻿using System;

namespace ShenNius.Auth.API.Domain.Entity.Common
{
    public interface IHasModifyTime
    {
        DateTime? ModifyTime { get; }
    }
}
