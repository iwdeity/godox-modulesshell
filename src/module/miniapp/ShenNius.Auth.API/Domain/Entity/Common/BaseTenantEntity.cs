﻿using SqlSugar;
using System;

/*************************************
* 类名：Common
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/3/30 17:24:54
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Auth.API.Domain.Entity.Common
{

    /// <summary>
    /// 所有多租户数据库实体基类
    /// </summary>
    public class BaseTenantEntity : IGlobalTenant, IEntity, IHasModifyTime, IHasCreateTime, IHasDeleteTime, ISoftDelete
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; protected set; }
        public int TenantId { get; set; }

        [SugarColumn(IsIgnore = true)]
        public string TenantName { get; set; }
        public DateTime? ModifyTime { get; protected set; }
        public DateTime CreateTime { get; protected set; } = DateTime.Now;
        public bool IsDeleted { get; private set; }
        public DateTime? DeleteTime { get; private set; }

        public void SoftDelete()
        {
            this.IsDeleted = true;
            this.DeleteTime = DateTime.Now;
        }
        public void NotifyModified()
        {
            this.ModifyTime = DateTime.Now;
        }

    }

    public class BaseTenantTreeEntity : BaseTenantEntity
    {
        // Desc:栏位集合    
        public string ParentList { get; protected set; }
        /// Desc:栏位等级     
        public int Layer { get; protected set; }
        public int ParentId { get; protected set; }
    }
}