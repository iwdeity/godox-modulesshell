﻿using System;

namespace ShenNius.Auth.API.Domain.Entity.Common
{
    public interface IHasDeleteTime
    {
        DateTime? DeleteTime { get; }
    }
}
