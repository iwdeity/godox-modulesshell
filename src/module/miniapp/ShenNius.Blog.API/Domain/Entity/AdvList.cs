﻿
using ShenNius.Auth.API.Domain.Entity.Common;
using ShenNius.Auth.API.Infrastructure.Enums.Extension;
using ShenNius.Blog.API.Domain.ValueObjects;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShenNius.Blog.API.Domain.Entity
{
    /// <summary>
    /// 广告位管理
    /// </summary>
    [SugarTable("Cms_AdvList")]
    public class AdvList : BaseTenantEntity, IAggregateRoot
    {
        /// <summary>
        /// Desc:广告位名称
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Desc:广告位类型
        /// Default:0
        /// Nullable:False
        /// </summary>
        public AdvEnum Type { get; private set; }

        [SugarColumn(IsIgnore = true)]
        public string TypeName { get; private set; }

        public void ConvertTypeName()
        {
            var dic = new Dictionary<AdvEnum, string>
            {
                { AdvEnum.FriendlyLink, AdvEnum.FriendlyLink.GetEnumText() },
                { AdvEnum.Slideshow, AdvEnum.Slideshow.GetEnumText() },
                { AdvEnum.GoodBlog, AdvEnum.GoodBlog.GetEnumText() },
                { AdvEnum.MiniApp, AdvEnum.MiniApp.GetEnumText() }
            };
            foreach (var item in dic.Where(item => item.Key == Type))
            {
                TypeName = item.Value;
            }
        }
        /// <summary>
        /// Desc:图片地址
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string ImgUrl { get; private set; }

        /// <summary>
        /// Desc:链接地址
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string LinkUrl { get; private set; }

        /// <summary>
        /// Desc:打开窗口类型
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Target { get; private set; }

        /// <summary>
        /// Desc:是否启用时间显示
        /// Default:b'0'
        /// Nullable:False
        /// </summary>
        public bool IsTimeLimit { get; private set; }

        /// <summary>
        /// Desc:开始时间
        /// Default:-
        /// Nullable:True
        /// </summary>
        public DateTime? BeginTime { get; private set; }

        /// <summary>
        /// Desc:结束时间
        /// Default:-
        /// Nullable:True
        /// </summary>
        public DateTime? EndTime { get; private set; }

        /// <summary>
        /// Desc:排序
        /// Default:0
        /// Nullable:False
        /// </summary>
        public int Sort { get; private set; }

        public string Summary { get; private set; }
    }
}
