﻿using ShenNius.Auth.API.Infrastructure.Configs;
using ShenNius.Auth.API.Infrastructure.Enums.Extension;
using ShenNius.Mall.API.Domain.Entity;
using ShenNius.Mall.API.Domain.Repository;
using ShenNius.Mall.API.Domain.ValueObjects;
using ShenNius.Repository;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/*************************************
* 类名：OrderGoodsService
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/20 11:53:40
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Mall.API.Infrastructure.Repository
{
    public class OrderGoodsRepository : BaseRepository<OrderGoods>, IOrderGoodsRepository
    {
        private readonly IBaseRepository<AppUserAddress> _appUserAddressService;
        private readonly IGoodsRepository _goodsRepository;

        public OrderGoodsRepository(ISqlSugarClient db, IBaseRepository<AppUserAddress> appUserAddressService, IGoodsRepository goodsRepository) : base(db)
        {
            _appUserAddressService = appUserAddressService;
            _goodsRepository = goodsRepository;
        }
        public async Task<ApiResult> ReceiptAsync(int orderId, int appUserId)
        {
            var order = new Order(); order.ChnageReceiptStatus();
            await db.Updateable<Order>().SetColumns(d => order).Where(d => d.Id == orderId && d.AppUserId.Equals(appUserId)).ExecuteCommandAsync();
            return new ApiResult();
        }
        public async Task<ApiResult> CancelOrderAsync(int orderId, int goodsId, int appUserId)
        {
            try
            {
                db.Ado.BeginTran();
                await db.Deleteable<OrderGoods>().Where(d => d.OrderId == orderId && d.GoodsId == goodsId && d.AppUserId == appUserId).ExecuteCommandAsync();
                var orderGoodsModel = await db.Queryable<OrderGoods>().Where(d => d.OrderId == orderId && d.AppUserId == appUserId).FirstAsync();
                if (orderGoodsModel == null)
                {
                    await db.Deleteable<Order>().Where(d => d.AppUserId == appUserId && d.Id == orderId).ExecuteCommandAsync();
                }
                db.Ado.CommitTran();
                return new ApiResult();
            }
            catch (Exception ex)
            {
                db.Ado.RollbackTran();
                return new ApiResult(ex.Message);
            }
        }

        public async Task<ApiResult> GetListAsync(int appUserId, string dataType)
        {
            var data = await db.Queryable<Order, OrderGoods>((o, od) => new object[] {
                JoinType.Inner,o.Id==od.OrderId,

            }).Where((o, od) => o.AppUserId.Equals(appUserId))
            .WhereIF(dataType == "payment", (o, od) => o.PayStatus == PayStatusEnum.WaitForPay.GetValue<int>())
            .WhereIF(dataType == "delivery", (o, od) => o.DeliveryStatus == DeliveryStatusEnum.WaitForSending.GetValue<int>() && o.PayStatus == PayStatusEnum.Paid.GetValue<int>())
            .WhereIF(dataType == "received", (o, od) => o.ReceiptStatus == ReceiptStatusEnum.WaitForReceiving.GetValue<int>() && o.PayStatus == PayStatusEnum.Paid.GetValue<int>() && o.DeliveryStatus == DeliveryStatusEnum.Sended.GetValue<int>())
            .Select((o, od) => new
            {
                o.CreateTime,
                o.OrderNo,
                od.TotalPrice,
                od.TotalNum,
                od.GoodsId,
                od.GoodsName,
                GoodsImg = od.ImgUrl,
                od.GoodsPrice,
                o.PayStatus,
                o.DeliveryStatus,
                o.ReceiptStatus,
                OrderId = o.Id,
                o.OrderStatus,
            }).ToListAsync();

            return new ApiResult(data);
        }


        /// <summary>
        /// 立马购买
        /// </summary>
        /// <param name="goodsId"></param>
        /// <param name="goodsNum"></param>
        /// <param name="specSkuId"></param>
        /// <param name="appUserId"></param>
        /// <returns></returns>
        public async Task<ApiResult> BuyNowAsync(int goodsId, int goodsNum, string specSkuId, int appUserId)
        {
            var goodsData = await _goodsRepository.GoodInfoIsExist(goodsId, goodsNum, specSkuId, appUserId);
            var addressModel = await _appUserAddressService.GetModelAsync(d => d.AppUserId == appUserId && d.IsDefault == true);

            try
            {
                db.Ado.BeginTran();
                Order order = new Order();
                order = order.BuildOrder(appUserId);
                var orderId = await db.Insertable(order).ExecuteReturnIdentityAsync();
                order.ChangeOrderId(orderId);
                OrderGoods orderGoods = order.BuildOrderGoods(goodsData.Item1, goodsData.Item2, goodsNum);
                await db.Insertable(orderGoods).ExecuteCommandAsync();
                var orderAddress = OrderAddress.Create(addressModel, orderId);
                await db.Insertable(orderAddress).ExecuteCommandAsync();

                //订单表统计最终支付的费用
                order.ChangePayPrice(orderGoods.TotalPrice);
                await db.Updateable<Order>(order).ExecuteCommandAsync();
                db.Ado.CommitTran();
                return new ApiResult();
            }
            catch (Exception ex)
            {
                db.Ado.RollbackTran();
                return new ApiResult(ex.Message);
            }
        }

        /// <summary>
        /// 购物车结算
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        public async Task<ApiResult> CartBuyAsync(int appUserId)
        {
            try
            {
                db.Ado.BeginTran();
                var cartList = await db.Queryable<Cart>().Where(d => d.AppUserId.Equals(appUserId)).ToListAsync();
                var inCludeGoods = cartList.Select(d => d.GoodsId).ToList();
                var goodsList = await _goodsRepository.GetListAsync(l => l.IsDeleted == true && inCludeGoods.Contains(l.Id));
                var addressModel = await _appUserAddressService.GetModelAsync(d => d.AppUserId == appUserId && d.IsDefault == true);
                Order order = new Order();
                order = order.BuildOrder(appUserId);
                var orderId = await db.Insertable(order).ExecuteReturnIdentityAsync();
                order.ChangeOrderId(orderId);

                var orderAddress = OrderAddress.Create(addressModel, orderId);
                await db.Insertable(orderAddress).ExecuteCommandAsync();

                int cartExistGoodsNum = 0; decimal totalPrice = 0, payPrice = 0;
                List<OrderGoods> orderGoodsList = new List<OrderGoods>();

                foreach (var item in cartList)
                {
                    cartExistGoodsNum = cartList.Where(d => d.GoodsId.Equals(item.Id)).Select(d => d.GoodsNum).FirstOrDefault();
                    var goodsData = await _goodsRepository.GoodInfoIsExist(item.GoodsId, cartExistGoodsNum, item.SpecSkuId, appUserId);
                    OrderGoods orderGoods = order.BuildOrderGoods(goodsData.Item1, goodsData.Item2, cartExistGoodsNum);
                    totalPrice += goodsData.Item2.GoodsPrice * cartExistGoodsNum;
                    payPrice += goodsData.Item2.GoodsPrice * cartExistGoodsNum;
                    orderGoodsList.Add(orderGoods);
                }

                await db.Insertable(orderGoodsList).ExecuteCommandAsync();

                //订单表统计最终支付的费用
                order.ChangePayPrice(totalPrice);
                await db.Updateable<Order>(order).ExecuteCommandAsync();

                db.Ado.CommitTran();
                return new ApiResult();
            }
            catch (Exception ex)
            {
                db.Ado.RollbackTran();
                throw new Exception(ex.ToString());
            }
        }
    }
}