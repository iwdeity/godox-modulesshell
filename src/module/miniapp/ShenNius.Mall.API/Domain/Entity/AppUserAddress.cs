﻿
using ShenNius.Auth.API.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Mall.API.Domain.Entity
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("shop_appuser_address")]
    public partial class AppUserAddress : BaseTenantEntity, IAggregateRoot
    {

        /// <summary>
        /// Desc:收货人姓名
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Name { get; private set; }

        /// <summary>
        /// Desc:联系电话
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Phone { get; private set; }

        /// <summary>
        /// Desc:所在省份id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public string Province { get; private set; }

        /// <summary>
        /// Desc:所在城市id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public string City { get; private set; }

        /// <summary>
        /// Desc:所在区id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public string Region { get; private set; }

        /// <summary>
        /// Desc:详细地址
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Detail { get; private set; }

        /// <summary>
        /// Desc:用户id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public int AppUserId { get; private set; }
        /// <summary>
        /// 是否默认地址 
        /// </summary>
        public bool IsDefault { get; private set; }

        public static AppUserAddress Create(int appUserId, string name, string detail, string phone, string province, string city, string region)
        {
            AppUserAddress appUserAddress = new AppUserAddress
            {
                AppUserId = appUserId,
                Name = name,
                Detail = detail,
                Phone = phone,
                Province = province,
                City = city,
                Region = region,
                IsDefault = false
            };
            return appUserAddress;

        }

        public void SetDefaultAddress(bool isDefault)
        {
            IsDefault = isDefault;
        }
        public AppUserAddress Modify(string name, string detail, string phone, string province, string city, string region)
        {
            Name = name;
            Detail = detail;
            Province = province;
            City = city;
            Region = region;
            Phone = phone;
            NotifyModified();
            return this;
        }
    }
}
