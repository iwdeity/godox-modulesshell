﻿
using ShenNius.Auth.API.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Mall.API.Domain.Entity
{
    ///<summary>
    ///订单地址表
    ///</summary>
    [SugarTable("shop_order_address")]
    public partial class OrderAddress : BaseTenantEntity
    {
        /// <summary>
        /// Desc:用户id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public int AppUserId { get; private set; }

        /// <summary>
        /// Desc:收货人姓名
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Name { get; private set; }

        /// <summary>
        /// Desc:联系电话
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Phone { get; private set; }

        /// <summary>
        /// Desc:所在省份id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public string Province { get; private set; }

        /// <summary>
        /// Desc:所在城市id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public string City { get; private set; }

        /// <summary>
        /// Desc:所在区id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public string Region { get; private set; }

        /// <summary>
        /// Desc:详细地址
        /// Default:
        /// Nullable:False
        /// </summary>           
        public string Detail { get; private set; }

        /// <summary>
        /// Desc:订单id
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public int OrderId { get; private set; }

        public static OrderAddress Create(AppUserAddress appUserAddress, int orderId)
        {
            OrderAddress orderAddress = new OrderAddress()
            {
                Name = appUserAddress.Name,
                Phone = appUserAddress.Phone,
                City = appUserAddress.City,
                Detail = appUserAddress.Detail,
                Region = appUserAddress.Region,
                AppUserId = appUserAddress.AppUserId,
                OrderId = orderId,
                Province = appUserAddress.Province
            };
            return orderAddress;
        }
    }
}
