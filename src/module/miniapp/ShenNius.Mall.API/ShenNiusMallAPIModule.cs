﻿using Microsoft.Extensions.DependencyInjection;
using ShenNius.Auth.API;
using ShenNius.Mall.API.Domain.Repository;
using ShenNius.Mall.API.Infrastructure.Repository;
using ShenNius.ModuleCore;
using ShenNius.ModuleCore.Context;

namespace ShenNius.Mall.API
{
    [DependsOn
       (typeof(ShenNiusAuthApiModule))]
    public class ShenNiusMallAPIModule : AppModule
    {
        public override void OnConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddScoped<IGoodsRepository, GoodsRepository>();
            context.Services.AddScoped<IOrderGoodsRepository, OrderGoodsRepository>();
            context.Services.AddScoped<IOrderRepository, OrderRepository>();
            context.Services.AddAutoMapper(typeof(AutomapperProfile));
        }
        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
        }
    }
}
