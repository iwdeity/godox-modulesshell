﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Shop.API.Domain.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Shop.Controllers
{
    [Area("shop")]
    public class OrderController : Controller
    {
        private readonly IOrderRepository _orderRepository;

        public OrderController(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Detail(int id)
        {
            var model = await _orderRepository.GetOrderDetailAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            return View(model.Data);
        }
    }
}
