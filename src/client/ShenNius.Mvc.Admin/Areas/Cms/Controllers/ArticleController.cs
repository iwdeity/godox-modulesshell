﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Cms.API.Domain.Entity;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Cms.Controllers
{
    [Area("cms")]
    public class ArticleController : Controller
    {
        private readonly IBaseRepository<Article> _articleService;

        public ArticleController(IBaseRepository<Article> articleService)
        {
            _articleService = articleService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Modify(int id = 0)
        {
            var model = id == 0 ? new Article() : await _articleService.GetModelAsync(d => d.Id == id && !d.IsDeleted);
            return View(model);
        }
    }
}
