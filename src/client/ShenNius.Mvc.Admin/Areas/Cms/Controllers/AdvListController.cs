﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Cms.API.Domain.Entity;
using ShenNius.Cms.API.Domain.ValueObjects.Enums;
using ShenNius.Repository;
using ShenNius.Sys.API.Infrastructure.Extension;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Cms.Controllers
{
    [Area("cms")]
    public class AdvListController : Controller
    {
        private readonly IBaseRepository<AdvList> _advListService;

        public AdvListController(IBaseRepository<AdvList> advListService)
        {
            _advListService = advListService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Modify(int id = 0)
        {
            AdvList model = id == 0 ? new AdvList() : await _advListService.GetModelAsync(d => d.Id == id && d.IsDeleted == false);

            Dictionary<int, string> dic = new Dictionary<int, string>
            {
                { AdvEnum.FriendlyLink.GetValue<int>(), AdvEnum.FriendlyLink.GetEnumText() },
                 { AdvEnum.Slideshow.GetValue<int>(), AdvEnum.Slideshow.GetEnumText() },
                  { AdvEnum.GoodBlog.GetValue<int>(), AdvEnum.GoodBlog.GetEnumText() },
                   { AdvEnum.MiniApp.GetValue<int>(), AdvEnum.MiniApp.GetEnumText() },
            };
            ViewBag.Dic = dic;
            return View(model);
        }
    }
}
