﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Repository;
using ShenNius.Sys.API.Domain.Entity;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Sys.Controllers
{
    [Area("sys")]
    public class TenantController : Controller
    {
        private readonly IBaseRepository<Tenant> _tenantService;

        public TenantController(IBaseRepository<Tenant> tenantService)
        {
            _tenantService = tenantService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        //
        [HttpGet]
        public async Task<IActionResult> Modify(int id = 0)
        {
            Tenant tenant = null;
            if (id == 0)
            {
                tenant = new Tenant();
            }
            else
            {
                tenant = await _tenantService.GetModelAsync(d => d.Id == id && d.IsDeleted == false);
            }
            return View(tenant);
        }
    }
}
