﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Repository;
using ShenNius.Sys.API.Domain.Entity;
using ShenNius.Sys.API.Domain.Repository;
using ShenNius.Sys.API.Domain.ValueObjects.Enum;
using ShenNius.Sys.API.Dtos.Output;
using ShenNius.Sys.API.Infrastructure;
using ShenNius.Sys.API.Infrastructure.Configs;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Sys.Controllers
{
    [Area("sys")]
    public class MenuController : Controller
    {
        private readonly IMenuRepository _menuRepository;
        private readonly IBaseRepository<Config> _configService;

        private readonly ICurrentUserContext _currentUserContext;

        public MenuController(IMenuRepository menuRepository, IBaseRepository<Config> configService, ICurrentUserContext currentUserContext)
        {
            _menuRepository = menuRepository;
            _configService = configService;
            _currentUserContext = currentUserContext;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Modify(int id)
        {
            MenuDetailOutput model = new MenuDetailOutput();
            string alreadyBtns = string.Empty;
            if (id > 0)
            {
                model.MenuOutput = await _menuRepository.GetModelAsync(d => d.Id == id);
                if (model.MenuOutput != null)
                {
                    if (model.MenuOutput.BtnCodeIds.Length > 0)
                    {
                        for (int i = 0; i < model.MenuOutput.BtnCodeIds.Length; i++)
                        {
                            alreadyBtns += model.MenuOutput.BtnCodeIds[i] + ",";
                        }
                        if (!string.IsNullOrEmpty(alreadyBtns))
                        {
                            alreadyBtns = alreadyBtns.TrimEnd(',');
                        }
                    }
                }
                ViewBag.AlreadyBtns = alreadyBtns;
            }
            else
            {
                model.MenuOutput = new Menu();
            }
            var configs = await _configService.GetListAsync(d => d.Type == nameof(ConfigEnum.Button) && d.IsDeleted == false);
            model.ConfigOutputs = configs;
            return View(model);
        }
    }
}
