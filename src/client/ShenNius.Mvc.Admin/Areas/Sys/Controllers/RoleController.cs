﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Repository;
using ShenNius.Sys.API.Domain.Entity;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Sys.Controllers
{
    [Area("sys")]
    public class RoleController : Controller
    {
        private readonly IBaseRepository<Role> _roleService;
        public RoleController(IBaseRepository<Role> roleService)
        {
            _roleService = roleService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        //
        [HttpGet]
        public async Task<IActionResult> Modify(int id = 0)
        {
            Role model = id == 0 ? new Role() : await _roleService.GetModelAsync(d => d.Id == id && d.IsDeleted == false);
            return View(model);
        }
        [HttpGet]
        public IActionResult SetMenu(int id)
        {
            ViewBag.RoleId = id;
            return View();
        }
    }
}
